
/**
 * @file
 * In this js get the Drupal.settings.boletines
 * to hide show Select options terms
 * Translate boletin = newsletter :)
 */
$(document).ready( function() {
  
  var boletines_terms = Drupal.settings.boletines.boletines_terms;
  var boletines_vid = Drupal.settings.boletines.boletines_vid;
  var boletines_terms_vid = Drupal.settings.boletines.boletines_terms_vid;
  var boletines_idiomas = Drupal.settings.boletines.boletines_idiomas;

  var id_boletines_vid = '#edit-taxonomy-'+boletines_vid;
  var id_boletines_terms_vid = '#edit-taxonomy-'+boletines_terms_vid+'-wrapper';
  var id_boletines_terms_selected = '#edit-taxonomy-'+boletines_terms_vid+' :selected';
  var id_boletines_idiomas_selected = '#edit-language :selected';
  var id_boletines_idiomas = '#edit-language-wrapper';

  if($(id_boletines_idiomas_selected).text() == '') {
    $(id_boletines_idiomas).css('display', 'none');
  }
  if($(id_boletines_terms_selected).text() == '') {
    $(id_boletines_terms_vid).css('display', 'none');
  };

  $(id_boletines_vid).change(
    function(){
      var terms_select = $(this).children("[@selected]").val();
      if (boletines_terms[terms_select]== terms_select){
        div = $(id_boletines_terms_vid);
        div.slideDown('slow');
      } else {
        $(id_boletines_terms_vid).css('display', 'none');  
      }
      if (boletines_idiomas[terms_select]== terms_select) {
        $(id_boletines_idiomas).slideDown('slow');
      } else {
        $(id_boletines_idiomas).css('display', 'none');  
      }
    }
  );
});
